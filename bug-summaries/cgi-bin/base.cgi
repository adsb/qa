#! /usr/bin/perl -Tw

require '/srv/bugs.qa.debian.org/perl/bug-summary.pl';

use CGI qw(:standard);

my $basepkgs = '/srv/bugs.qa.debian.org/data/lists/basedebs';
my %basepkgs = map { binary_to_source($_) => 1 } read_package_list ($basepkgs);
my @basepkgs = sort keys %basepkgs;

html_header ('Base system bugs');
show_table (@basepkgs);
html_footer ();
