BEGIN;

SET search_path TO apt;

-- distribution table

DELETE FROM apt.distribution;

COPY apt.distribution (archive, suite, distribution) FROM STDIN WITH DELIMITER ' ';
debian squeeze oldstable
debian squeeze-proposed-updates oldstable-proposed-updates
debian-security squeeze oldstable-security
debian-backports squeeze-backports oldstable-backports
debian-backports squeeze-backports-sloppy oldstable-backports-sloppy
debian wheezy stable
debian wheezy-proposed-updates stable-proposed-updates
debian-security wheezy stable-security
debian wheezy-updates stable-updates
debian wheezy-backports stable-backports
debian jessie testing
debian jessie-proposed-updates testing-proposed-updates
debian-security jessie testing-security
debian sid unstable
debian experimental experimental
\.

COMMIT;
